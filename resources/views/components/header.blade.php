<div>
    <!-- No surplus words or unnecessary actions. - Marcus Aurelius -->
    <h1>{{ $title }}</h1>
    <hr />
    <p>{{ $name }}</p>
</div>
